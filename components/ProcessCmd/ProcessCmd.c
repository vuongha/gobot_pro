/*
 * ProcessCmd.c
 *
 *  Created on: Nov 9, 2018
 *      Author: Mr Vuong
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"



typedef enum {
	RUN_COMPLETE ,
	FAIL_HEAD,
	FAIL_BACK,
	IMPACT_HEAD,
	IMPACT_BACK
} state_when_move;



typedef struct {
	int mask_block;    // ma khoi lenh
	int value_position_left;  // vi tri va chieu cua motor left
	int value_position_right;  // as above
} parameter_block;


int num_spot(char *param, int len) {
	int i = 0;
	int value = 0;

	for (i = 0; i < len; i++) {
		if (param[i] == ',')
			value++;
	}
	return value;
}

void find_index_spot(char *param, int len, int *index) {
	int i = 0;

	for (i = 0; i < len; i++) {
		if (param[i] == ',') {
			*(index++) = i;
		}
	}
}

void execute_fix_cmd(char const *ptr_char, int len) {

	int num_of_spot = 0;
//	int num_of_block = 0;


	char *array_block = (char*) calloc(len, sizeof(char));
	if (array_block == NULL)
		return;

	memcpy(array_block, ptr_char, len);

	num_of_spot = num_spot(array_block, len);

	int *index_spot = (int*) calloc(num_of_spot, sizeof(int));

	find_index_spot((char*) array_block, len, index_spot);

	int *block = calloc(num_of_spot + 1, sizeof(int));

	for (int i = 0; i <= num_of_spot; i++) {
		sscanf((char*) (array_block + index_spot[i] + 1), "%d,", &block[i]);
		printf("block[i] is %d \n", block[i]);
	}

	for (int i = 0; i <= num_of_spot; i++) {
		switch (block[i]) {
		case 200:


			break;
		case 300:


			break;
		case 400:


			break;
		case 500:


			break;
		default:
			break;
		}

//		if(fail_back == true){
//			fail_back = false;
//			reserver();
//			light_move_backward();
//			free(block);
//			free(index_spot);
//			return;
//		}
//		if(fail_head  == true){
//			fail_back = false;
//			forward();
//			light_move_backward();
//			free(block);
//			free(index_spot);
//			return;
//		}



	}
}

void huhu(void){
	printf("huhu\n");
}

