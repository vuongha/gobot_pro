/*
 * ProcessCmd.h
 *
 *  Created on: Nov 9, 2018
 *      Author: Mr Vuong
 */

#ifndef COMPONENTS_PROCESSCMD_PROCESSCMD_H_
#define COMPONENTS_PROCESSCMD_PROCESSCMD_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"


void huhu(void);

extern void execute_fix_cmd(char const *ptr_char, int len);

#endif /* COMPONENTS_PROCESSCMD_PROCESSCMD_H_ */
