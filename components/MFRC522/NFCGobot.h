/*
 * NFCGobot.h
 *
 *  Created on: Dec 25, 2018
 *      Author: TA QUOC ANH
 */

#ifndef COMPONENTS_MFRC522_NFCGOBOT_H_
#define COMPONENTS_MFRC522_NFCGOBOT_H_

#include <Arduino.h>
#include "MFRC522.h"

class NFCGobot  : public MFRC522  {
public:

	int readBlock(void);

	NFCGobot();

	NFCGobot(uint8_t ss, uint8_t rst) : MFRC522(ss, rst) {}

	virtual ~NFCGobot();
};

#endif /* COMPONENTS_MFRC522_NFCGOBOT_H_ */
