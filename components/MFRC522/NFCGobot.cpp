/*
 * NFCGobot.cpp
 *
 *  Created on: Dec 25, 2018
 *      Author: TA QUOC ANH
 */

#include "NFCGobot.h"

NFCGobot::NFCGobot() {
	// TODO Auto-generated constructor stub

}

NFCGobot::~NFCGobot() {
	// TODO Auto-generated destructor stub
}

int NFCGobot:: readBlock(){

	MFRC522::MIFARE_Key key;

	for (byte i = 0; i < 6; i++)
		key.keyByte[i] = 0xFF;
	String  blockgobot;
 	byte block;
	byte len;
	MFRC522::StatusCode status;
	if (PICC_IsNewCardPresent() && PICC_ReadCardSerial()){

		byte buffer[2];
		block = 4;
		// read only tow byte from card
		len = 2;
		// check authenticate
		status = PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 4,
				&key, &(uid));
		if (status != MFRC522::STATUS_OK) {
			Serial.print(F("Authentication failed: "));
			Serial.println(GetStatusCodeName(status));
			// return 0 is fall
			return 0;
		}

		// get cmd from card NFC
		status = MIFARE_Read(block, buffer, &len);
		if (status != MFRC522::STATUS_OK) {
			Serial.print(F("Reading failed: "));
			Serial.println(GetStatusCodeName(status));
			// return 0 is fall
			return 0;
		}

		// convert buffer variable in byte to string
		blockgobot.getBytes(buffer, 2,0);


	}
	return blockgobot.toInt();
}


