/*
 * GBBLightRGB.h
 *
 *  Created on: Oct 8, 2018
 *      Author: Mr Vuong
 */

#ifndef GBBLIGHTRGB_GBBLIGHTRGB_H_
#define GBBLIGHTRGB_GBBLIGHTRGB_H_

#include <stdint.h>
#include <driver/rmt.h>
#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>

#include <freertos/task.h>

typedef struct {
	uint8_t red;
	uint8_t green;
	uint8_t blue;
} pixel_t;

typedef enum{
	ADVERIESING =1,
	CONNECTED   =2,
	DISCONNECTED =3,
	COMPLETE   =4,
	RED = 5,
	GREEN = 6,
	BLUE = 7,

} type_show;

class GBBLightRGB {

public:
	GBBLightRGB();

	void show(void);  // display on LED

	void setColorOrder(char *colorOrder);

	void setPixel (uint32_t index, uint8_t red, uint8_t green, uint8_t blue	);

	void setPixel (uint32_t index, pixel_t pixel);

	void clear(void);

	void runAdvertising(int color);

	void runConnected(void);

     void runRGB(type_show type, int time);

     type_show image;
     int time;
     TaskHandle_t isRunLed;
     TaskHandle_t isRunNormal;

	virtual ~GBBLightRGB();

private:


	char *colorOrder;
	uint16_t pixelCount;
	rmt_channel_t channel;
	rmt_item32_t *items;
	pixel_t *pixels;




	   // type of LED showing
         // time showing of type


};

extern GBBLightRGB RobotLED;



#endif /* GBBLIGHTRGB_GBBLIGHTRGB_H_ */
