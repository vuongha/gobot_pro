/*
 * GBBLightRGB.cpp
 *
 *  Created on: Oct 8, 2018
 *      Author: Mr Vuong
 */

#include "GBBLightRGB.h"

#include <freertos/FreeRTOS.h>

#include <freertos/task.h>

#include <esp_log.h>
#include <driver/rmt.h>
#include <driver/gpio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdexcept>
#include <esp_heap_caps.h>
#include "sdkconfig.h"

static char tag[] = "WS2812";

static void SetItem1(rmt_item32_t *pItem) {
	assert(pItem != nullptr);
	pItem->level0 = 1;
	pItem->duration0 = 10;
	pItem->level1 = 0;
	pItem->duration1 = 6;
}

static void SetItem0(rmt_item32_t *pItem) {
	assert(pItem != nullptr);
	pItem->level0 = 1;
	pItem->duration0 = 4;
	pItem->level1 = 0;
	pItem->duration1 = 8;
}

static void SetTerminator(rmt_item32_t *pItem) {
	assert(pItem != nullptr);
	pItem->level0 = 0;
	pItem->duration0 = 0;
	pItem->level1 = 0;
	pItem->duration1 = 0;
}

static uint8_t getChannelValueByType(char type, pixel_t pixel) {
	switch (type) {
	case 'r':
	case 'R':
		return pixel.red;
	case 'b':
	case 'B':
		return pixel.blue;
	case 'g':
	case 'G':
		return pixel.green;
	}
	ESP_LOGW(tag, "Unknown color channel 0x%2x", type);
	return 0;
}

GBBLightRGB RobotLED;

static void runRGBLed(void *param);

void static run_LED(void * param);

GBBLightRGB::GBBLightRGB() {

	this->pixelCount = 8;
	this->channel = RMT_CHANNEL_0;
	this->items = new rmt_item32_t[8 * 24 + 1];
	this->pixels = new pixel_t[8];
	this->colorOrder = (char *) "GRB";
	this->time =0;
	rmt_config_t config_rmt_module;

	config_rmt_module.rmt_mode = RMT_MODE_TX;
	config_rmt_module.channel = this->channel; // 8 pixel
	config_rmt_module.gpio_num = GPIO_NUM_18;   // GPIO18 is to control RGB LED  //tron
//	 config_rmt_module.gpio_num = GPIO_NUM_25; // GPIO18 is to control RGB LED  //vuong
	config_rmt_module.mem_block_num = 8 - this->channel;
	config_rmt_module.clk_div = 8;
	config_rmt_module.tx_config.loop_en = 0;
	config_rmt_module.tx_config.carrier_en = 0;
	config_rmt_module.tx_config.idle_output_en = 1;
	config_rmt_module.tx_config.idle_level = (rmt_idle_level_t) 0;
	config_rmt_module.tx_config.carrier_freq_hz = 10000;
	config_rmt_module.tx_config.carrier_level = (rmt_carrier_level_t) 1;
	config_rmt_module.tx_config.carrier_duty_percent = 50;

	ESP_ERROR_CHECK(rmt_config(&config_rmt_module));
	ESP_ERROR_CHECK(rmt_driver_install(this->channel, 0, 0));

	xTaskCreate(runRGBLed, "run led", 4096, NULL, 2, &RobotLED.isRunLed);
	xTaskCreate(run_LED, "run normal", 4096, NULL, 2, &RobotLED.isRunNormal);

}

void GBBLightRGB::show(void) {
	auto pCurrentItem = this->items;

	for (auto i = 0; i < this->pixelCount; i++) {
		uint32_t currentPixel = (getChannelValueByType(this->colorOrder[0],
				this->pixels[i]) << 16)
				| (getChannelValueByType(this->colorOrder[1], this->pixels[i])
						<< 8)
				| (getChannelValueByType(this->colorOrder[2], this->pixels[i]));

		ESP_LOGD(tag, "Pixel value: %x", currentPixel);
		for (int j = 23; j >= 0; j--) {
			// We have 24 bits of data representing the red, green amd blue channels. The value of the
			// 24 bits to output is in the variable current_pixel.  We now need to stream this value
			// through RMT in most significant bit first.  To do this, we iterate through each of the 24
			// bits from MSB to LSB.
			if (currentPixel & (1 << j)) {
				SetItem1(pCurrentItem);
			} else {
				SetItem0(pCurrentItem);
			}
			pCurrentItem++;
		}
	}
	SetTerminator(pCurrentItem); // Write the RMT terminator.

	// Show the pixels.
	ESP_ERROR_CHECK(
			rmt_write_items(this->channel, this->items, this->pixelCount * 24,
					1 /* wait till done */));

}

void GBBLightRGB::setColorOrder(char *colorOrder) {

	if (colorOrder != nullptr && strlen(colorOrder) == 3) {
		this->colorOrder = colorOrder;
	}

}

void GBBLightRGB::setPixel(uint32_t index, uint8_t red, uint8_t green,
		uint8_t blue) {

	assert(index < 8);
	this->pixels[index].red = red;
	this->pixels[index].green = green;
	this->pixels[index].blue = blue;

}

void GBBLightRGB::setPixel(uint32_t index, pixel_t pixel) {

	assert(index < 8);

	this->pixels[index] = pixel;

}

void GBBLightRGB::clear(void) {

	for (int i = 0; i <= 8; i++) {
		this->pixels[i].red = 0;
		this->pixels[i].green = 0;
		this->pixels[i].blue = 0;
	}
}

GBBLightRGB::~GBBLightRGB() {

	delete this->items;
	delete this->pixels;

} /* namespace Gobot */

void GBBLightRGB::runRGB(type_show type_, int count) {


	if (eTaskGetState(isRunLed) == eSuspended) {
		image = type_;
		time = count;
		printf("loop %d \n", count);

		vTaskSuspend(isRunNormal);
		vTaskResume(isRunLed);
	}

}

static void runRGBLed(void *param) {
	vTaskSuspend(NULL);
	for (;;) {

		switch (RobotLED.image) {

		case ADVERIESING: // run advertising
			printf("loop %d \n", RobotLED.time);
			for (int i = 0; i < RobotLED.time; i++) {


				RobotLED.setPixel(0, 100, 0, 0);
				RobotLED.setPixel(1, 0, 0, 0);
				RobotLED.setPixel(2, 0, 0, 0);
				RobotLED.setPixel(3, 0, 0, 0);
				RobotLED.setPixel(4, 0, 0, 0);
				RobotLED.setPixel(5, 0, 0, 0);
				RobotLED.setPixel(6, 0, 0, 0);
				RobotLED.setPixel(7, 100, 0, 0);
				RobotLED.show();
				vTaskDelay(10);

				RobotLED.setPixel(0, 100, 100, 0);
				RobotLED.setPixel(1, 100, 100, 0);
				RobotLED.setPixel(2, 0, 0, 0);
				RobotLED.setPixel(3, 0, 0, 0);
				RobotLED.setPixel(4, 0, 0, 0);
				RobotLED.setPixel(5, 0, 0, 0);
				RobotLED.setPixel(6, 100, 100, 0);
				RobotLED.setPixel(7, 100, 100, 0);
				RobotLED.show();
				vTaskDelay(10);

				RobotLED.setPixel(0, 100, 100, 0);
				RobotLED.setPixel(1, 100, 100, 0);
				RobotLED.setPixel(2, 100, 50, 50);
				RobotLED.setPixel(3, 0, 0, 0);
				RobotLED.setPixel(4, 0, 0, 0);
				RobotLED.setPixel(5, 100, 50, 50);
				RobotLED.setPixel(6, 100, 100, 0);
				RobotLED.setPixel(7, 100, 100, 0);
				RobotLED.show();
				vTaskDelay(10);

				RobotLED.setPixel(0, 100, 100, 0);
				RobotLED.setPixel(1, 100, 100, 0);
				RobotLED.setPixel(2, 100, 50, 50);
				RobotLED.setPixel(3, 100, 10, 40);
				RobotLED.setPixel(4, 100, 10, 40);
				RobotLED.setPixel(5, 100, 50, 50);
				RobotLED.setPixel(6, 100, 100, 0);
				RobotLED.setPixel(7, 100, 100, 0);
				RobotLED.show();
				vTaskDelay(10);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(10);

			}
			vTaskResume(RobotLED.isRunNormal);
			vTaskSuspend(NULL);
			break;

		case RED: // con connected

			for (int i = 0; i < RobotLED.time; i++) {


				RobotLED.setPixel(0, 100, 0, 00);
				RobotLED.setPixel(1, 100, 0, 00);
				RobotLED.setPixel(2, 100, 0, 00);
				RobotLED.setPixel(3, 100, 0, 00);
				RobotLED.setPixel(4, 100, 0, 00);
				RobotLED.setPixel(5, 100, 0, 00);
				RobotLED.setPixel(6, 100, 0, 0);
				RobotLED.setPixel(7, 100, 0, 00);
				RobotLED.show();

			}

			vTaskSuspend(NULL);

			break;

		case DISCONNECTED:

			for (int i = 0; i < RobotLED.time; i++) {
				i++;

				RobotLED.setPixel(0, 100, 0, 0);
				RobotLED.setPixel(1, 0, 0, 0);
				RobotLED.setPixel(2, 0, 0, 0);
				RobotLED.setPixel(3, 0, 0, 0);
				RobotLED.setPixel(4, 0, 0, 0);
				RobotLED.setPixel(5, 0, 0, 0);
				RobotLED.setPixel(6, 0, 0, 0);
				RobotLED.setPixel(7, 100, 0, 0);
				RobotLED.show();
				vTaskDelay(10);

				RobotLED.setPixel(0, 100, 100, 0);
				RobotLED.setPixel(1, 100, 100, 0);
				RobotLED.setPixel(2, 0, 0, 0);
				RobotLED.setPixel(3, 0, 0, 0);
				RobotLED.setPixel(4, 0, 0, 0);
				RobotLED.setPixel(5, 0, 0, 0);
				RobotLED.setPixel(6, 100, 100, 0);
				RobotLED.setPixel(7, 100, 100, 0);
				RobotLED.show();
				vTaskDelay(10);

				RobotLED.setPixel(0, 100, 100, 0);
				RobotLED.setPixel(1, 100, 100, 0);
				RobotLED.setPixel(2, 100, 50, 50);
				RobotLED.setPixel(3, 0, 0, 0);
				RobotLED.setPixel(4, 0, 0, 0);
				RobotLED.setPixel(5, 100, 50, 50);
				RobotLED.setPixel(6, 100, 100, 0);
				RobotLED.setPixel(7, 100, 100, 0);
				RobotLED.show();
				vTaskDelay(10);

				RobotLED.setPixel(0, 100, 100, 0);
				RobotLED.setPixel(1, 100, 100, 0);
				RobotLED.setPixel(2, 100, 50, 50);
				RobotLED.setPixel(3, 100, 10, 40);
				RobotLED.setPixel(4, 100, 10, 40);
				RobotLED.setPixel(5, 100, 50, 50);
				RobotLED.setPixel(6, 100, 100, 0);
				RobotLED.setPixel(7, 100, 100, 0);
				RobotLED.show();
				vTaskDelay(10);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(10);

			}

			vTaskSuspend(NULL);
			break;
		case COMPLETE:

			for (int i = 0; i < RobotLED.time; i++) {
				i++;

				RobotLED.setPixel(0, 0, 0, 255);
				RobotLED.setPixel(1, 0, 0, 255);
				RobotLED.setPixel(2, 0, 0, 255);
				RobotLED.setPixel(3, 0, 0, 255);
				RobotLED.setPixel(4, 0, 0, 255);
				RobotLED.setPixel(5, 0, 0, 255);
				RobotLED.setPixel(6, 0, 0, 255);
				RobotLED.setPixel(7, 0, 0, 255);
				RobotLED.show();
				vTaskDelay(20);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(20);

			}
			vTaskResume(RobotLED.isRunNormal);
			vTaskSuspend(NULL);
			break;

		case GREEN:

			for (int i = 0; i < RobotLED.time; i++) {


				RobotLED.setPixel(0, 0, 100, 00);
				RobotLED.setPixel(1, 0, 100, 00);
				RobotLED.setPixel(2, 0, 100, 00);
				RobotLED.setPixel(3, 0, 100, 00);
				RobotLED.setPixel(4, 0, 100, 00);
				RobotLED.setPixel(5, 0, 100, 00);
				RobotLED.setPixel(6, 0, 100, 0);
				RobotLED.setPixel(7, 0, 100, 00);
				RobotLED.show();

			}

			vTaskSuspend(NULL);

			break;

		case BLUE:

			for (int i = 0; i < RobotLED.time; i++) {


				RobotLED.setPixel(0, 0, 0, 100);
				RobotLED.setPixel(1, 0, 0, 100);
				RobotLED.setPixel(2, 0, 0, 100);
				RobotLED.setPixel(3, 0, 0, 100);
				RobotLED.setPixel(4, 0, 0, 100);
				RobotLED.setPixel(5, 0, 0, 100);
				RobotLED.setPixel(6, 0, 0, 100);
				RobotLED.setPixel(7, 0, 0, 100);
				RobotLED.show();

			}

			vTaskSuspend(NULL);

			break;

		default:

			break;

		}

	}

}

void static run_LED(void * param) {
	short red = 0;
	short blue = 0;
	short green = 0;
	vTaskSuspend(NULL);
	for (;;) {
		red = red + 3;
		if (red > 125)
			red = 30;
		blue = blue + 5;
		if (blue > 180)
			blue = 75;
		green = green + 7;
		if (green > 225)
			green = 7;

		RobotLED.setPixel(0, red + 5, green + 2, blue + 3);
		RobotLED.setPixel(1, red + 15, green, blue+5);
		RobotLED.setPixel(2, red + 10, green + 9, blue);
		RobotLED.setPixel(3, red + 3, green + 5, blue);
		RobotLED.setPixel(4, red + 8, green, blue+5);
		RobotLED.setPixel(5, red + 2, green + 5, blue+9);
		RobotLED.setPixel(6, red + 5, green + 1, blue);
		RobotLED.setPixel(7, red + 4, green, blue);
		RobotLED.show();
		vTaskDelay(30);

	}
}

