/*
 * GBBSensor.h
 *
 *  Created on: Oct 18, 2018
 *      Author: Mr Vuong
 */

#ifndef COMPONENTS_GBBSENSOR_GBBSENSOR_H_
#define COMPONENTS_GBBSENSOR_GBBSENSOR_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "driver/gpio.h"
#include "driver/adc.h"

class GBBSensor {
public:
	GBBSensor(adc1_channel_t channle);
	GBBSensor(adc2_channel_t channle);
	int getValueADC(adc1_channel_t channle);
	int getValueADC(adc2_channel_t channle);


private:
	adc1_channel_t channel_1;
	adc2_channel_t channel_2;


	virtual ~GBBSensor();
};

#endif /* COMPONENTS_GBBSENSOR_GBBSENSOR_H_ */
