/*
 * GBBSensor.cpp
 *
 *  Created on: Oct 18, 2018
 *      Author: Mr Vuong
 */

#include "GBBSensor.h"

GBBSensor::GBBSensor(adc1_channel_t channle) {

	this->channel_1 = channle;
	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(this->channel_1, ADC_ATTEN_DB_11);
	adc_power_on();

}

GBBSensor::GBBSensor(adc2_channel_t channle) {
	this->channel_2 = channle;
	adc2_config_channel_atten(this->channel_2, ADC_ATTEN_DB_11);
	adc_power_on();
}

int GBBSensor::getValueADC(adc1_channel_t channle) {
	return adc1_get_raw(channle);

}

int GBBSensor::getValueADC(adc2_channel_t channle){
	static int valueADC;
	adc2_get_raw(this->channel_2,ADC_WIDTH_BIT_12,&valueADC);
	return valueADC;

}


GBBSensor::~GBBSensor() {
	// TODO Auto-generated destructor stub
}

