/*
 * GBBodyWheels.h
 *
 *  Created on: Oct 9, 2018
 *      Author: Mr Vuong
 */

#ifndef GBBODYWHEELS_GBBODYWHEELS_H_
#define GBBODYWHEELS_GBBODYWHEELS_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "esp_attr.h"
#include "soc/rtc.h"
#include "driver/mcpwm.h"
#include "soc/mcpwm_reg.h"
#include "soc/mcpwm_struct.h"
#include  <driver/timer.h>
#include "soc/timer_group_struct.h"

typedef enum {
	WHEEL_RIGHT = 0, WHEEL_LEFT = 1
} wheel_type;

typedef enum {
	FORWARD = 0, BACKWARD = 1
} direct_type;

typedef struct {
	uint32_t capture_signal;
	mcpwm_capture_signal_t sel_cap_signal;
} capture;



class GBBodyWheels {
public:

	bool isRuning(void);

	void runWithSpeed(wheel_type wheel, direct_type direct,
			uint32_t speed);
	void setSpeed(wheel_type wheel, uint32_t speed);

	void stop(wheel_type wheel);

	void moveWithDistance(direct_type direct,uint32_t distance);

	void rotateRightWithAngle(uint32_t angle);

	void rotateLeftWithAngle(uint32_t angle);

	void turnLeft(void);

	void turnRight(void);



	GBBodyWheels();

	virtual ~GBBodyWheels();



private:


	void initMotorControl(void);

	static void setDutyPWM(wheel_type wheel, float duty_cycle);

	static void controlWheelRight(void *param);

	static void controlWheelLeft(void *param);

	static void  IRAM_ATTR intrHandleEncoder(void *param);


//	static void IRAM_ATTR intrHandleEncoder(void *param);

};

#endif /* GBBODYWHEELS_GBBODYWHEELS_H_ */
