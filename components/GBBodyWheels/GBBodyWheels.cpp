/*
 * GBBodyWheels.cpp
 *
 *  Created on: Oct 9, 2018
 *      Author: Mr Vuong
 */

#include "GBBodyWheels.h"


// old version
//#define PWM_WHEEL_LEFT_IO15   15
//#define DIREC_WHEEL_LEFT_IO22 22
//#define DIREC_WHEEL_LEFT_IO21 21
//#define ENCODER_WHEEL_LEFT_IO27 27
//
//#define PWM_WHEEL_RIGHT_IO16 16
//#define DIREC_WHEEL_RIGHT_IO19 19
//#define DIREC_WHEEL_RIGHT_IO23 23
//#define ENCODER_WHEEL_RIGHT_IO26 26

 //new verison
//#define PWM_WHEEL_LEFT_IO15   15
//#define DIREC_WHEEL_LEFT_IO22 22
//#define DIREC_WHEEL_LEFT_IO21  17
//#define ENCODER_WHEEL_LEFT_IO27 35
//
//#define PWM_WHEEL_RIGHT_IO16 16
//#define DIREC_WHEEL_RIGHT_IO19 5
//#define DIREC_WHEEL_RIGHT_IO23 21
//#define ENCODER_WHEEL_RIGHT_IO26 39
//
//#define STANDBY_IO14 12



// backup

//
#define PWM_WHEEL_LEFT_IO15   15
#define DIREC_WHEEL_LEFT_IO22 12
#define DIREC_WHEEL_LEFT_IO21 14
#define ENCODER_WHEEL_LEFT_IO27 22

#define PWM_WHEEL_RIGHT_IO16 16
#define DIREC_WHEEL_RIGHT_IO19 21
#define DIREC_WHEEL_RIGHT_IO23 19
#define ENCODER_WHEEL_RIGHT_IO26 26

#define STANDBY_IO14 32


//backup


#define CAP_SIG_NUM 2
#define CAP0_INT_EN BIT(27)  //Capture 0 interrupt bit
#define CAP1_INT_EN BIT(28)  //Capture 1 interrupt bit

	static mcpwm_dev_t *MCPWM[2] = { &MCPWM0, &MCPWM1 };

	static float speedWheelLeftPoint;
	static uint32_t numOfPulseWheelLeftPoint;
	static bool isControlPositionLeft;
	static uint32_t pulseOfEncoderLeft;
	static SemaphoreHandle_t isCompletedWheelLeft;
	static TaskHandle_t xHandlePIDLeft;

	static uint32_t oldPusleEnoderLeft = 0;


	static float speedWheelRightPoint;
	static uint32_t numOfPulseWheelRightPoint;
	static bool isControlPositionRight;
	static uint32_t pulseOfEncoderRight;
	static SemaphoreHandle_t isCompletedWheelRight;
	static TaskHandle_t xHandlePIDRight;

	static uint32_t oldPusleEnoderRight = 0;



 void IRAM_ATTR GBBodyWheels::intrHandleEncoder(void *param) {

	uint32_t mcpwm_intr_status;
	mcpwm_intr_status = MCPWM[MCPWM_UNIT_0]->int_st.val;

	if (mcpwm_intr_status & CAP0_INT_EN) {
		pulseOfEncoderLeft++;

		if (isControlPositionLeft) {
			numOfPulseWheelLeftPoint--;

			if (numOfPulseWheelLeftPoint <= 0) {
				isControlPositionLeft = false;
				oldPusleEnoderLeft =0;
				pulseOfEncoderLeft =0;

				gpio_set_level((gpio_num_t) DIREC_WHEEL_LEFT_IO22, 0);
				gpio_set_level((gpio_num_t) DIREC_WHEEL_LEFT_IO21, 0);
			//	setDutyPWM(WHEEL_LEFT,0);
				xSemaphoreGiveFromISR(isCompletedWheelLeft, NULL);
				mcpwm_stop(MCPWM_UNIT_0, MCPWM_TIMER_0);

				vTaskSuspend(xHandlePIDLeft);



			}
		}
	}

	if (mcpwm_intr_status & CAP1_INT_EN) {
		pulseOfEncoderRight++;

		if (isControlPositionRight) {
			numOfPulseWheelRightPoint--;

			if (numOfPulseWheelRightPoint <= 0) {
				isControlPositionRight = false;

				pulseOfEncoderRight = 0;
				oldPusleEnoderRight =0;

				gpio_set_level((gpio_num_t) DIREC_WHEEL_RIGHT_IO19, 0);
				gpio_set_level((gpio_num_t) DIREC_WHEEL_RIGHT_IO23, 0);
			//	setDutyPWM(WHEEL_RIGHT,0);

				xSemaphoreGiveFromISR(isCompletedWheelRight, NULL);

				mcpwm_stop(MCPWM_UNIT_0, MCPWM_TIMER_1);
				vTaskSuspend(xHandlePIDRight);



			}
		}
	}
	MCPWM[MCPWM_UNIT_0]->int_clr.val = mcpwm_intr_status;
}



bool GBBodyWheels::isRuning(void) {
	if ((eTaskGetState(xHandlePIDLeft) != eSuspended)
			|| eTaskGetState(xHandlePIDRight) != eSuspended)
		return true;
	else
		return false;
}



void GBBodyWheels::setSpeed(wheel_type wheel, uint32_t speed){
	switch (wheel) {
	case WHEEL_LEFT:
		speedWheelLeftPoint = speed;
		break;
	case WHEEL_RIGHT:
		speedWheelRightPoint = speed;

		break;
	}

}



void GBBodyWheels::runWithSpeed(wheel_type wheel, direct_type direct,
		uint32_t speed) {
	if (speed > 40)
		speed = 40;

	switch (wheel) {
	case WHEEL_LEFT:

		speedWheelLeftPoint = speed;
		if (direct == FORWARD) {
			gpio_set_level((gpio_num_t) DIREC_WHEEL_LEFT_IO22, 1);
			gpio_set_level((gpio_num_t) DIREC_WHEEL_LEFT_IO21, 0);
		} else if (direct == BACKWARD) {
			gpio_set_level((gpio_num_t) DIREC_WHEEL_LEFT_IO22, 0);
			gpio_set_level((gpio_num_t) DIREC_WHEEL_LEFT_IO21, 1);
		}
		vTaskResume(xHandlePIDLeft); // alow task PID run
		break;
	case WHEEL_RIGHT:

		speedWheelRightPoint = speed;
		if (direct == FORWARD) {
			gpio_set_level((gpio_num_t) DIREC_WHEEL_RIGHT_IO19, 1);
			gpio_set_level((gpio_num_t) DIREC_WHEEL_RIGHT_IO23, 0);
		} else if (direct == BACKWARD) {
			gpio_set_level((gpio_num_t) DIREC_WHEEL_RIGHT_IO19, 0);
			gpio_set_level((gpio_num_t) DIREC_WHEEL_RIGHT_IO23, 1);
		}
		vTaskResume(xHandlePIDRight); // alow task PID run
		break;
	}
	gpio_set_level((gpio_num_t) STANDBY_IO14, 1);
}

void GBBodyWheels::stop(wheel_type wheel) {
	switch (wheel) {
	case WHEEL_LEFT:

		gpio_set_level((gpio_num_t) DIREC_WHEEL_LEFT_IO22, 0);
		gpio_set_level((gpio_num_t) DIREC_WHEEL_LEFT_IO21, 0);
		mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A);
		mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, 0);
		mcpwm_stop(MCPWM_UNIT_0, MCPWM_TIMER_0);

		isControlPositionLeft = false;
		numOfPulseWheelLeftPoint = 0;
		oldPusleEnoderLeft =0;
		vTaskSuspend(xHandlePIDLeft);
		break;

	case WHEEL_RIGHT:
		gpio_set_level((gpio_num_t) DIREC_WHEEL_RIGHT_IO19, 0);
		gpio_set_level((gpio_num_t) DIREC_WHEEL_RIGHT_IO23, 0);
		mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_OPR_A);
		mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_OPR_A, 0);
		mcpwm_stop(MCPWM_UNIT_0, MCPWM_TIMER_1);

		isControlPositionRight = false;
		numOfPulseWheelRightPoint = 0;
		oldPusleEnoderRight =0;
		vTaskSuspend(xHandlePIDRight);
		break;
	}
}

void GBBodyWheels::moveWithDistance(direct_type direct, uint32_t distance) {

	switch (direct) {
	case FORWARD:

		isControlPositionLeft = true;
		isControlPositionRight = true;
		numOfPulseWheelLeftPoint = distance * 47;
		numOfPulseWheelRightPoint = distance * 47;
		runWithSpeed(WHEEL_LEFT, FORWARD, 10);
		runWithSpeed(WHEEL_RIGHT, FORWARD, 10);
		break;
	case BACKWARD:
		isControlPositionLeft = true;
		isControlPositionRight = true;
		numOfPulseWheelLeftPoint = distance * 47;
		numOfPulseWheelRightPoint = distance * 47;
		runWithSpeed(WHEEL_LEFT, BACKWARD, 10);
		runWithSpeed(WHEEL_RIGHT, BACKWARD, 10);
		break;
	}


	xSemaphoreTake(isCompletedWheelLeft, portMAX_DELAY);
	xSemaphoreTake(isCompletedWheelRight, portMAX_DELAY);
	stop(WHEEL_LEFT);
	stop(WHEEL_RIGHT);
}

void GBBodyWheels::rotateLeftWithAngle(uint32_t angle) {
	isControlPositionLeft = true;
	isControlPositionRight = true;
	numOfPulseWheelLeftPoint = angle * 3.8;
	numOfPulseWheelRightPoint = angle * 3.8;

	runWithSpeed(WHEEL_LEFT, FORWARD, 10);
	runWithSpeed(WHEEL_RIGHT, BACKWARD, 10);

	xSemaphoreTake(isCompletedWheelLeft, portMAX_DELAY);
	xSemaphoreTake(isCompletedWheelRight, portMAX_DELAY);
	stop(WHEEL_LEFT);
	stop(WHEEL_RIGHT);
}

/**
	* rotate robot with angle
 */

void GBBodyWheels::rotateRightWithAngle(uint32_t angle) {
	isControlPositionLeft = true;
	isControlPositionRight = true;
	numOfPulseWheelLeftPoint = angle * 3.8;
	numOfPulseWheelRightPoint = angle * 3.8;

	runWithSpeed(WHEEL_LEFT, BACKWARD, 10);
	runWithSpeed(WHEEL_RIGHT, FORWARD, 10);

	xSemaphoreTake(isCompletedWheelLeft, portMAX_DELAY);
	xSemaphoreTake(isCompletedWheelRight, portMAX_DELAY);

	stop(WHEEL_LEFT);
	stop(WHEEL_RIGHT);

}

GBBodyWheels::GBBodyWheels() {

	initMotorControl(); // install gpio and pwm to control motor

}

void GBBodyWheels::controlWheelLeft(void *param) {

	static float Kp = 0.05;
	static float Kd = 0.05;
	static float Ki = 0.01;
	static float err = 0;
	static float oldErr = 0;

	static float intergral = 0;
	static float derivative = 0;


	static float gain = 0;
	static float dt = 3;

	vTaskSuspend(NULL);
	for (;;) {


		err = (float) (speedWheelLeftPoint
				- (pulseOfEncoderLeft - oldPusleEnoderLeft));
		//printf("speed Left %d \n",pulseOfEncoderLeft - oldPusleEnoderLeft);


		intergral = intergral + err*dt;
		derivative = (err - oldErr)/dt;
		gain = Kp*err + Ki*intergral + Kd*derivative;

		oldPusleEnoderLeft = pulseOfEncoderLeft;
		oldErr = err;

//		if (gain < 0)
//			gain = -(gain);
//		if (gain > 100)
//			gain = 100;


		if(numOfPulseWheelLeftPoint<70){
			speedWheelLeftPoint = 5;

		}
		else if(numOfPulseWheelLeftPoint<30) {
			speedWheelLeftPoint = 2;

		}
		else {
			setDutyPWM(WHEEL_LEFT, gain);
		}
		vTaskDelay(dt);


	}
}


void GBBodyWheels::controlWheelRight(void *param) {
	static float Kp = 0.05;
	static float Kd = 0.05;
	static float Ki = 0.01;
	static float err = 0;
	static float oldErr = 0;

	static float intergral = 0;
	static float derivative = 0;


	static float gain = 0;
	static float dt = 3;
	vTaskSuspend(NULL);
	for (;;) {

	//	printf("dutt %f \n",mcpwm_get_duty(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_OPR_A));
		//printf(" pPart: %f iPart: %f dPart: %f \n",Kp*err,Ki*intergral,Kd*derivative);

		err = (float) (speedWheelRightPoint
				- (pulseOfEncoderRight - oldPusleEnoderRight));

		printf("speed right     : %d \n",pulseOfEncoderRight - oldPusleEnoderRight);

		intergral = intergral + err*dt;
		derivative = (err - oldErr)/dt;

		gain = Kp*err + Ki*intergral + Kd*derivative;

		oldErr = err;
		oldPusleEnoderRight = pulseOfEncoderRight;
//		if (gain < 0)
//			gain = -(gain);
//		if (gain > 100)
//			gain = 100;


	//	printf("gain left : %f \n",gain );
		if(numOfPulseWheelRightPoint<70){
			speedWheelRightPoint = 5;

		}
		else if(numOfPulseWheelRightPoint<30){
			speedWheelRightPoint = 2;

		}
		else {


		setDutyPWM(WHEEL_RIGHT, gain);

		printf(" gain: %f \n",gain);
		}
		vTaskDelay(dt);
	}
}



void GBBodyWheels::setDutyPWM(wheel_type wheel, float duty_cycle) {
	switch (wheel) {
	case WHEEL_LEFT:
		mcpwm_set_frequency(MCPWM_UNIT_0,MCPWM_TIMER_0,5000);
		mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A,
				MCPWM_DUTY_MODE_0);
		mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, duty_cycle);
		mcpwm_start(MCPWM_UNIT_0, MCPWM_TIMER_0);
		break;

	case WHEEL_RIGHT:
		mcpwm_set_frequency(MCPWM_UNIT_0,MCPWM_TIMER_1,5000);
		mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_OPR_A,
				MCPWM_DUTY_MODE_0);
		mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_OPR_A, duty_cycle);
		mcpwm_start(MCPWM_UNIT_0, MCPWM_TIMER_1);
		break;
	}
}

void GBBodyWheels::initMotorControl(void) {
	mcpwm_config_t pwmConfig;
	gpio_config_t gpioConfig;

	isCompletedWheelLeft = xSemaphoreCreateBinary();
	isCompletedWheelRight = xSemaphoreCreateBinary();

	xTaskCreate(controlWheelLeft, "Task PID wheel Left", 4096, NULL, 3,
			&xHandlePIDLeft);
	xTaskCreate(controlWheelRight, "Task PID wheel right", 4096, NULL, 3,
			&xHandlePIDRight);

	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, PWM_WHEEL_LEFT_IO15);
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM1A, PWM_WHEEL_RIGHT_IO16);
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM_CAP_0, ENCODER_WHEEL_LEFT_IO27);
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM_CAP_1, ENCODER_WHEEL_RIGHT_IO26);
	gpio_pulldown_en((gpio_num_t) ENCODER_WHEEL_LEFT_IO27);
	gpio_pulldown_en((gpio_num_t) ENCODER_WHEEL_RIGHT_IO26);

	pwmConfig.frequency = 1000;
	pwmConfig.cmpr_a = 0;
	pwmConfig.cmpr_b = 0;
	pwmConfig.counter_mode = MCPWM_UP_COUNTER;
	pwmConfig.duty_mode = MCPWM_DUTY_MODE_1; // high pulse
	mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwmConfig);
	mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_1, &pwmConfig);
	mcpwm_capture_enable(MCPWM_UNIT_0, MCPWM_SELECT_CAP0, MCPWM_POS_EDGE, 0);
	mcpwm_capture_enable(MCPWM_UNIT_0, MCPWM_SELECT_CAP1, MCPWM_POS_EDGE, 0);
	MCPWM[MCPWM_UNIT_0]->int_ena.val = (CAP0_INT_EN | CAP1_INT_EN);
	mcpwm_isr_register(MCPWM_UNIT_0, intrHandleEncoder, NULL,
	ESP_INTR_FLAG_IRAM, NULL);

	// Config gpio to
	gpioConfig.intr_type = GPIO_INTR_DISABLE;
	gpioConfig.mode = GPIO_MODE_OUTPUT;

//	 backup
	gpioConfig.pin_bit_mask = GPIO_SEL_21 | GPIO_SEL_14 | GPIO_SEL_12
			| GPIO_SEL_19 | GPIO_SEL_32;

//	// new version
//	gpioConfig.pin_bit_mask = GPIO_SEL_17 | GPIO_SEL_22 | GPIO_SEL_5
//			| GPIO_SEL_21 | GPIO_SEL_12;


	gpio_config(&gpioConfig);

}

GBBodyWheels::~GBBodyWheels() {
	// TODO Auto-generated destructor stub
}



