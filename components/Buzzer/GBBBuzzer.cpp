/*
 * GBBBuzzer.cpp
 *
 *  Created on: Mar 6, 2019
 *      Author: TA QUOC ANH
 */

#include <freertos/FreeRTOS.h>

#include <freertos/task.h>

#include "GBBBuzzer.h"

static void runTaskSound(void *param);

xQueueHandle Queue_Recive_Midi;

GBBBuzzer::GBBBuzzer() {
	// TODO Auto-generated constructor stub
	Queue_Recive_Midi = xQueueCreate(2, sizeof(Recive_Midi));
	xTaskCreate(runTaskSound, "runtask", 4096, NULL, 2, NULL);

}


//PWM speaker(GPIO_NUM_27); // new version
PWM speaker(GPIO_NUM_13); // backup
void GBBBuzzer::play_file(midi_data *data, int length) {

	for (int i = 0; i < length; i++) {

		play_note(data[i].note, data[i].length_note);
	}
}

void GBBBuzzer::play_file(Recive_Midi *param) {

	for (int i = 0; i < param->num_of_note; i++) {
		play_note(param->p_midi_data[i].note,
				param->p_midi_data[i].length_note);
		printf("hello vuong %d \n", param->p_midi_data[i].note);
	}
}

void GBBBuzzer::play_file__array(midi_data *param, int length) {

	static Recive_Midi Var;
	Var.p_midi_data = param;

	Var.num_of_note = length;
	xQueueSend(Queue_Recive_Midi, &Var, 10);
}

void GBBBuzzer::play_note(notefreqs note, length speed) {

	if (note == mute) {
		printf("hello fail\n");
		speaker.stop(0);

		vTaskDelay(6 * speed);
	} else {
		speaker.setDuty(2000);
		speaker.setFrequency(note);
		vTaskDelay(6 * speed);
		speaker.stop(0);

	}

}


midi_data happy[24] = { { G3, not_don }, { G3, not_don },

{ A3, not_den }, { G3, not_den }, { C4, not_den },

{ B3, not_trang }, { G3, not_don }, { G3, not_don },

{ A3, not_den }, { G3, not_den }, { D4, not_den },

{ C4, not_trang },

{ G3, not_don }, { G3, not_don }, { F4, not_den }, { E4, not_den }, { C4,
		not_den }, { B3, not_den },

{ G3, not_den },

{ G3, not_don }, { mute, not_den },

{ C4, not_den }, { C4, not_den }, { G3, not_den },

};

midi_data bipbip[8] = { { C4, not_don }, { mute, not_nua_don }, { C4, not_don },
		{ C4, not_den }, { E3, not_den }, { C4, not_den }, { E3, not_den }, {
				C4, not_den }, };
//
//midi_data start[10] = { { E3, not_nua_don }, { mute, not_nua_nua_don }, { G3,
//		not_nua_don }, { mute, not_nua_nua_don }, { C4, not_nua_don }, { mute,
//		not_nua_nua_don }, { C4, not_nua_don }, { mute, not_nua_nua_don }, { C4,
//		not_den}, { mute, not_nua_nua_don } };

midi_data start[12] = { { G4, not_nua_don }, { mute, not_nua_nua_don }, { A4,
		not_nua_don }, { mute, not_nua_nua_don }, { G4, not_don }, { mute,
		not_nua_nua_don }, { G4, not_nua_don }, { mute, not_nua_nua_don }, { E4,
		not_den }, { mute, not_nua_nua_don }, { G4, not_don }, { mute,
		not_nua_nua_don }, };

midi_data connected[5] = { { D4, not_nua_don }, { mute, not_nua_nua_don }, { F4,
		not_nua_don }, { mute, not_nua_nua_don }, { D4, not_nua_don }, };

midi_data disconnected[7] = { { C4, not_nua_don }, { mute, not_nua_nua_don }, {
		C4, not_nua_don }, { mute, not_nua_nua_don }, { C4, not_nua_don }, {
		mute, not_nua_nua_don }, { G3_, not_trang }, };

midi_data chicken_dacne[50] = {
		{ G3, not_nua_don }, { mute, not_nua_nua_don },

		{ G3, not_nua_don }, { mute, not_nua_nua_don },


		{ A3, not_nua_don }, { mute, not_nua_nua_don },

		{ A3, not_nua_don }, { mute, not_nua_nua_don },


		{ E3, not_nua_don }, { mute, not_nua_nua_don },

		{ E3, not_nua_don }, { mute, not_nua_nua_don },


		{ G3, not_don }, { mute, not_nua_nua_don },


		{ G3, not_nua_don }, { mute, not_nua_nua_don },

		{ G3, not_nua_don }, { mute, not_nua_nua_don },


		{ A3, not_nua_don }, { mute, not_nua_nua_don },

		{ A3, not_nua_don }, { mute, not_nua_nua_don },


		{ E3, not_nua_don }, { mute, not_nua_nua_don },

		{ E3, not_nua_don }, { mute, not_nua_nua_don },


		{ G3, not_don }, { mute, not_nua_nua_don },



		{ G3, not_nua_don }, { mute, not_nua_nua_don },

		{ G3, not_nua_don }, { mute, not_nua_nua_don },


		{ A3, not_nua_don }, { mute, not_nua_nua_don },

		{ A3, not_nua_don }, { mute, not_nua_nua_don },


		{ C4, not_nua_don }, { mute, not_nua_nua_don },

		{ C4, not_nua_don }, { mute, not_nua_nua_don },


		{ B3, not_don }, { mute, not_nua_nua_don },


		{ B3, not_don }, { B3, not_nua_nua_don },

		{ A3, not_don }, { A3, not_nua_nua_don },

		{ G3, not_don }, { G3, not_nua_nua_don },

		{ F3, not_don }, { F3, not_nua_nua_don },






};

GBBBuzzer::~GBBBuzzer() {
	// TODO Auto-generated destructor stub
}

// task run file midi
static void runTaskSound(void *param) {
	Recive_Midi Var;
	for (;;) {
		xQueueReceive(Queue_Recive_Midi, &Var, portMAX_DELAY);
		printf("hello\n");
		RobotBuzzer.play_file(&Var);

	}
}

GBBBuzzer RobotBuzzer;
