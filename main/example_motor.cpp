/*
 * Display.c
 *
 *  Created on: 14.08.2017
 *      Author: darek
 */
#include "iostream"
#include <string.h>
#include <stdio.h>
#include "driver/gpio.h"
#include <driver/i2c.h>
#include <esp_log.h>
#include <esp_err.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "mpu6050.h"
#include "MPU6050_6Axis_MotionApps20.h"
#include "sdkconfig.h"
#include "Kalman.h"
#include <stdio.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include "GBBLightRGB.h"
#include "GBBodyWheels.h"

#include "GBBBuzzer.h"

//#include "ProcessCmd.h"

using namespace std;

typedef struct {
	int mask_block;
	int value_block;
	int value_block2;
} param_block;

GBBodyWheels Gobot;

int flag_stop = 0;

typedef enum {
	RUN_COMPLETE, FAIL_HEAD, FAIL_BACK, IMPACT_HEAD, IMPACT_BACK
} state_when_move;

typedef struct {
	int mask_block;    // ma khoi lenh
	int value_position_left;  // vi tri va chieu cua motor left
	int value_position_right;  // as above
} parameter_block;

void soundMidi(void) {

	gpio_pad_select_gpio(GPIO_NUM_27);
	/* Set the GPIO as a push/pull output */
	gpio_set_direction(GPIO_NUM_27, GPIO_MODE_OUTPUT);

	/* Blink off (output low) */
	for (int i = 0; i < 5; i++) {

		gpio_set_level(GPIO_NUM_27, 1);
		vTaskDelay(200 / portTICK_PERIOD_MS);
		/* Blink on (output high) */
		gpio_set_level(GPIO_NUM_27, 0);
		vTaskDelay(100 / portTICK_PERIOD_MS);
	}

}

void dancing(void) {
	for (int i = 0; i < 12; i++) {
		Gobot.rotateRightWithAngle(15);
		vTaskDelay(5);
		Gobot.rotateLeftWithAngle(15);
		vTaskDelay(5);
	}
}



int num_spot(char *param, int len) {
	int i = 0;
	int value = 0;

	for (i = 0; i < len; i++) {
		if (param[i] == ',')
			value++;
	}
	return value;
}
// ham tim do dau ';'
int num_spot_2(char *param, int len) {
	int i = 0;
	int value = 0;

	for (i = 0; i < len; i++) {
		if (param[i] == ';')
			value++;
	}
	return value;
}

void find_index_spot(char *param, int len, int *index) {
	int i = 0;

	for (i = 0; i < len; i++) {
		if (param[i] == ',') {
			*(index++) = i;
		}
	}
}
// ham tim index dau ";"
void find_index_spot_2(char *param, int len, int *index) {
	int i = 0;

	for (i = 0; i < len; i++) {
		if (param[i] == ';') {
			*(index++) = i;
		}
	}
}

void execute_fix_cmd(char const *ptr_char, int len) {

	int num_of_spot = 0;
//	int num_of_block = 0;

	char *array_block = (char*) calloc(len, sizeof(char));
	if (array_block == NULL)
		return;

	memcpy(array_block, ptr_char, len);

	num_of_spot = num_spot(array_block, len);

	int *index_spot = (int*) calloc(num_of_spot, sizeof(int));

	find_index_spot((char*) array_block, len, index_spot);

	int *block = (int*) calloc(num_of_spot + 1, sizeof(int));

	for (int i = 0; i <= num_of_spot; i++) {
		sscanf((char*) (array_block + index_spot[i] + 1), "%d,", &block[i]);
		printf("block[i] is %d \n", block[i]);
	}

	for (int i = 0; i <= num_of_spot; i++) {
		switch (block[i]) {
		case 200:

			Gobot.rotateLeftWithAngle(90);
			vTaskDelay(1000);
			break;
		case 300:

			Gobot.rotateLeftWithAngle(90);
			vTaskDelay(1000);
			break;
		case 400:
			Gobot.moveWithDistance(FORWARD, 10);
			vTaskDelay(1000);

			break;
		case 500:

			Gobot.moveWithDistance(BACKWARD, 10);
			vTaskDelay(1000);
			break;
		default:
			break;
		}

//		if(fail_back == true){
//			fail_back = false;
//			reserver();
//			light_move_backward();
//			free(block);phy_init: wifi mac time delta
//			free(index_spot);
//			return;
//		}
//		if(fail_head  == true){
//			fail_back = false;
//			forward();
//			light_move_backward();
//			free(block);
//			free(index_spot);
//			return;
//		}

	}
}

void execute_fix_cmd3(char *ptr_char, int len) {

	int mask;
	int value_1;
	int value_2;
	int value_3;
	static char *array_block;
	int num_of_block = 0;

	array_block = ptr_char;
//	char *array_block = (char*) calloc(len, sizeof(char));
//	if (array_block == NULL)
//		return;
//	memcpy(array_block, ptr_char, len);

	for (int i = 0; i < len; i++) {
		if (array_block[i] == ';')
			num_of_block++;
	}

	int *index_block = (int*) calloc(num_of_block, sizeof(int));

	find_index_spot_2((char*) array_block, len, index_block);

	for (int i = 0; i < num_of_block - 2; i++) {
		printf("index block %d \n", index_block[i]);

		sscanf((char*) (array_block + index_block[i]), ";%d", &mask);

//		if (flag_stop ==1 ){ // dung robot khan cap
//			flag_stop =0;
//			Gobot.stop(WHEEL_LEFT);
//			Gobot.stop(WHEEL_RIGHT);
//
//			return;
//		}

		switch (mask) {
		case 200:

			sscanf((char*) (array_block + index_block[i] + 5), "%d,%d;",
					&value_1, &value_2);
			if (value_1 > 0)
				Gobot.moveWithDistance(FORWARD, 20); // value_1
			else if (value_1 < 0)
				Gobot.moveWithDistance(BACKWARD, 20); // value_1
			//RobotBuzzer.play_file__array(bipbip,5);
			RobotLED.runRGB(COMPLETE, 4);
			RobotBuzzer.play_file__array(bipbip, 3);
			vTaskDelay(100);
			break;

		case 210:
			sscanf((char*) (array_block + index_block[i] + 5), "%d,", &value_1);
			if (value_1 > 0)
				Gobot.rotateRightWithAngle(value_1);
			else if (value_1 < 0)
				Gobot.rotateLeftWithAngle(-(value_1));

			RobotLED.runRGB(COMPLETE, 4);
			RobotBuzzer.play_file__array(bipbip, 3);

			vTaskDelay(100);
			break;
		case 900:   // dancing
			RobotBuzzer.play_file__array(chicken_dacne, 50);
			RobotLED.runRGB(ADVERIESING, 14);
			dancing();

			printf("soundMidi\n");
			break;

		case 400:    // LED show
			RobotLED.runRGB(ADVERIESING, 10);

			break;
		case 300:

			break;

		default:
			break;

		}
	}
	free(index_block);

}

void execute_fix_cmd2(char const *ptr_char, int len) {
	int num_of_spot = 0;
	int num_of_block = 0;

	int i = 0;

	char *array_block = (char*) calloc(len, sizeof(char));
	if (array_block == NULL)
		return;
	memcpy(array_block, ptr_char, len);

	num_of_spot = num_spot_2(array_block, len);

	printf("len = %d\n", len);

	num_of_block = (num_of_spot - 1) / 3;

	int *index_spot = (int*) calloc(num_of_spot, sizeof(int));

	find_index_spot_2((char*) array_block, len, index_spot);

	param_block *block = (param_block *) calloc(num_of_block,
			sizeof(param_block));
	printf("so block %d\n", num_of_spot);
	printf("so block %d\n", num_of_block);

	for (i = 0; i < num_of_block; i++) {
		sscanf((char*) (array_block + index_spot[3 * i] + 1), "%d;%d;%d",
				&block[i].mask_block, &block[i].value_block,
				&block[i].value_block2);
	}

	for (int i = 0; i < num_of_block; i++) {
		switch (block[i].mask_block) {
		case 200:
			if (block[i].value_block > 0)
				Gobot.moveWithDistance(FORWARD, block[i].value_block);
			else if (block[i].value_block < 0)
				Gobot.moveWithDistance(BACKWARD, -(block[i].value_block));
			vTaskDelay(100);
			break;
		case 210:
			if (block[i].value_block > 0)
				Gobot.rotateRightWithAngle(block[i].value_block);
			else if (block[i].value_block < 0)
				Gobot.rotateLeftWithAngle(-(block[i].value_block));
			vTaskDelay(100);
			break;
		case 202:
			/// code am thanh
			soundMidi();
			printf("soundMidi\n");
			break;
		case 300:
			/// code anh sang
			printf("light\n");
			break;

		}
	}

	free(array_block);
	free(index_spot);
	free(block);

}

#define SERVICE_UUID         "6261CB01-F5EA-490E-8548-E6821E3E7792"
#define SERVICE_UUID_AD      "62610000-f5ea-490e-8548-e6821e3e7792"

#define CHAR_BLOCK_UNIT      "62614C14-F5EA-490E-8548-E6821E3E7792"  // block
#define CHAR_BLOCK_PROGRAM   "6261EC06-F5EA-490E-8548-E6821E3E7792"

typedef struct {
	char *prepare_buf;
	int prepare_len;
} Recive_Block;

xQueueHandle Queue_Recive_Block;

xQueueHandle Queue_Recive_Unit;

int flag_not_next_block = 1;

BLECharacteristic *pCharacteristic_Unit;
BLECharacteristic *pCharacteristic_Program;

class MyCallbacks1: public BLECharacteristicCallbacks {

	void onWrite(BLECharacteristic *pCharacteristic) {
		Recive_Block Var;
		std::string value = pCharacteristic->getValue();

		printf("call back 1 \n");
		printf("data back 1 is %s :", value.c_str());

		if (flag_not_next_block == 1) {

			//	char const *data = value.c_str();

			char *memory = (char*) calloc(value.length() + 1, sizeof(char));

			memcpy(memory, value.c_str(), value.length() + 1);

			Var.prepare_buf = memory;

			Var.prepare_len = value.length() + 1;

			printf("data is bef %s\n", memory);

			xQueueSend(Queue_Recive_Block, &Var, 100);
		}
	}
};

static void run_block(void *param) {

	Recive_Block Var;

	for (;;) {
		xQueueReceive(Queue_Recive_Block, &Var, portMAX_DELAY);

		flag_not_next_block = 0;
		printf("data queue  %s\n", Var.prepare_buf);
		execute_fix_cmd3(Var.prepare_buf, Var.prepare_len); //run cmd at here
		free(Var.prepare_buf);
		flag_not_next_block = 1;

	}
}

class MyCallbacks2: public BLECharacteristicCallbacks {
	void onWrite(BLECharacteristic *pCharacteristic) {

		std::string value = pCharacteristic->getValue();

		Recive_Block Var;

		printf("call back 2 \n");

		printf("data back 2 is %s \n", value.c_str());

		if (flag_not_next_block == 1) {
			char *data = (char*) calloc(value.length() + 1, sizeof(char));

			memcpy(data, value.c_str(), value.length() + 1);

			Var.prepare_buf = data;

			Var.prepare_len = value.length() + 1;

			printf("data is %s\n", data);

			xQueueSend(Queue_Recive_Unit, &Var, 100);
		}

	}
};

class CallbackConnect: public BLEServerCallbacks {
	void onDisconnect(BLEServer *pBLEServer) {
		RobotLED.runRGB(RED, 4);
		printf("disconnect\n");
		pCharacteristic_Program->setValue("R");
		RobotBuzzer.play_file__array(disconnected, 7);

	}
	void onConnect(BLEServer *pBLEServer) {
		printf("connect\n");
		RobotLED.runRGB(BLUE, 4);
		RobotBuzzer.play_file__array(connected, 5);

	}
};

static void run_block_unit(void *pram) {
	Recive_Block Var;
	param_block value_block;
	for (;;) {
		xQueueReceive(Queue_Recive_Unit, &Var, portMAX_DELAY);

		flag_not_next_block = 0;

		sscanf((char*) Var.prepare_buf, "%d,%d,%d", &value_block.mask_block,

		&value_block.value_block, &value_block.value_block2);

		switch (value_block.mask_block) {
		case 200:
			if (value_block.value_block > 0)
				Gobot.moveWithDistance(FORWARD, 20); //value_block.value_block
			else if (value_block.value_block < 0)
				Gobot.moveWithDistance(BACKWARD, -(-20)); //value_block.value_block
			RobotLED.runRGB(COMPLETE, 4);
			RobotBuzzer.play_file__array(bipbip, 3);
			vTaskDelay(100);

			break;
		case 210:
			if (value_block.value_block > 0)
				Gobot.rotateRightWithAngle(90); //value_block.value_block
			else if (value_block.value_block < 0)
				Gobot.rotateLeftWithAngle(-(-90)); //value_block.value_block
			RobotLED.runRGB(COMPLETE, 4);
			RobotBuzzer.play_file__array(bipbip, 3);
			vTaskDelay(100);
			break;
		case 900:
			RobotBuzzer.play_file__array(chicken_dacne, 50);
			RobotLED.runRGB(ADVERIESING, 14);
			dancing();
			printf("soundMidi\n");
			break;

		case 300:

			break;
		case 400:
			RobotLED.runRGB(ADVERIESING, 10);
			break;

		default:
			break;
		}
		free(Var.prepare_buf);
		flag_not_next_block = 1;
	}

}

void testBLE(void) {

	Queue_Recive_Block = xQueueCreate(2, sizeof(Recive_Block));

	Queue_Recive_Unit = xQueueCreate(2, sizeof(Recive_Block));

	xTaskCreate(run_block, "run_program ", 4096, NULL, 4, NULL);

	xTaskCreate(run_block_unit, "run_block", 4096, NULL, 4, NULL);

	BLEDevice::init("HTV");

	BLEServer *pServer = BLEDevice::createServer();

	pServer->setCallbacks(new CallbackConnect());

	BLEService *pService = pServer->createService(BLEUUID(SERVICE_UUID));

	pCharacteristic_Program = pService->createCharacteristic(

	CHAR_BLOCK_PROGRAM,
			BLECharacteristic::PROPERTY_READ
					| BLECharacteristic::PROPERTY_WRITE);

	pCharacteristic_Unit = pService->createCharacteristic(
	CHAR_BLOCK_UNIT,
			BLECharacteristic::PROPERTY_READ
					| BLECharacteristic::PROPERTY_WRITE);

	pCharacteristic_Program->setValue("B");

	pCharacteristic_Program->setCallbacks(new MyCallbacks1());

	pCharacteristic_Unit->setCallbacks(new MyCallbacks2());

	pService->start();

	BLEAdvertising *pAdvertising = pServer->getAdvertising();

	pAdvertising->addServiceUUID(BLEUUID(SERVICE_UUID_AD));

	pAdvertising->start();

}

void test_motor(void) {
	for (int i = 0; i < 10; i++) {

		Gobot.moveWithDistance(FORWARD, 20);
		vTaskDelay(1000);
	}

}
