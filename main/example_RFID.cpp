/*
 * example_mfrc522.cpp
 *
 *  Created on: Dec 19, 2018
 *      Author: TA QUOC ANH
 */

#include <SPI.h>
#include <MFRC522.h>

#include "NFCGobot.h"
#include "driver/uart.h"
#define SS_PIN 2
#define RST_PIN 4

MFRC522 mfrc522(SS_PIN, RST_PIN);

NFCGobot Gobot1(SS_PIN,RST_PIN);


void RFID_duminfor(void) {
	Serial.begin(115200);		// Initialize serial communications with the PC
	while (!Serial)
		;

	// Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
	SPI.begin(18, 19, 23, 2);			// Init SPI bus
	mfrc522.PCD_Init();		// Init MFRC522
	mfrc522.PCD_DumpVersionToSerial();// Show details of PCD - MFRC522 Card Reader details
	Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));
	while (1) {
		// Look for new cards
		if (mfrc522.PICC_IsNewCardPresent()) {
			printf("have PICC_IsNewCardPresent \n ");
		}

		// Select one of the cards
		if (mfrc522.PICC_ReadCardSerial()) {
			printf("have PICC_ReadCardSerial \n ");
			mfrc522.PICC_DumpToSerial(&(mfrc522.uid));
		}
		if (mfrc522.PICC_ReadCardSerial()) {

			printf("hello anh em \n ");
			mfrc522.PICC_DumpToSerial(&(mfrc522.uid));
		}
	}
}

MFRC522::MIFARE_Key key;
byte nuidPICC[4];

void printHex(byte *buffer, byte bufferSize);
void printDec(byte *buffer, byte bufferSize);

void read_UID(void) {
	Serial.begin(115200);
	SPI.begin(18, 19, 23, 2);	 // Init SPI bus
	mfrc522.PCD_Init(); // Init MFRC522

	for (byte i = 0; i < 6; i++) {
		key.keyByte[i] = 0xFF;
	}

	Serial.println(F("This code scan the MIFARE Classsic NUID."));
	Serial.print(F("Using the following key:"));
	printHex(key.keyByte, MFRC522::MF_KEY_SIZE);

	while (1) {
		// Look for new cards
//		  if ( ! mfrc522.PICC_IsNewCardPresent())
//		    break;

		// Verify if the NUID has been readed
		if (mfrc522.PICC_ReadCardSerial() && mfrc522.PICC_IsNewCardPresent()) {
			Serial.print(F("PICC type: "));
			MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
			Serial.println(mfrc522.PICC_GetTypeName(piccType));

			// Check is the PICC of Classic MIFARE type
			if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI
					&& piccType != MFRC522::PICC_TYPE_MIFARE_1K
					&& piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
				Serial.println(F("Your tag is not of type MIFARE Classic."));
				return;
			}

			if (mfrc522.uid.uidByte[0] != nuidPICC[0]
					|| mfrc522.uid.uidByte[1] != nuidPICC[1]
					|| mfrc522.uid.uidByte[2] != nuidPICC[2]
					|| mfrc522.uid.uidByte[3] != nuidPICC[3]) {
				Serial.println(F("A new card has been detected."));

				// Store NUID into nuidPICC array
				for (byte i = 0; i < 4; i++) {
					nuidPICC[i] = mfrc522.uid.uidByte[i];
				}

				Serial.println(F("The NUID tag is:"));
				Serial.print(F("In hex: "));
				printHex(mfrc522.uid.uidByte, mfrc522.uid.size);
				Serial.println();
				Serial.print(F("In dec: "));
				printDec(mfrc522.uid.uidByte, mfrc522.uid.size);
				Serial.println();
			} else
				Serial.println(F("Card read previously."));

			// Halt PICC
			mfrc522.PICC_HaltA();

			// Stop encryption on PCD
			mfrc522.PCD_StopCrypto1();
		}

	}

}

/**
 * Helper routine to dump a byte array as hex values to Serial.
 */
void printHex(byte *buffer, byte bufferSize) {
	for (byte i = 0; i < bufferSize; i++) {
		Serial.print(buffer[i] < 0x10 ? " 0" : " ");
		Serial.print(buffer[i], HEX);
	}
}

/**
 * Helper routine to dump a byte array as dec values to Serial.
 */
void printDec(byte *buffer, byte bufferSize) {
	for (byte i = 0; i < bufferSize; i++) {
		Serial.print(buffer[i] < 0x10 ? " 0" : " ");
		Serial.print(buffer[i], DEC);
	}
}

void write_infor(void) {
	Serial.begin(115200);		// Initialize serial communications with the PC
	while (!Serial)
		;

	// Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
	SPI.begin(18, 19, 23, 2);			// Init SPI bus
	mfrc522.PCD_Init();		// Init MFRC522
	mfrc522.PCD_DumpVersionToSerial();// Show details of PCD - MFRC522 Card Reader details
	Serial.println(F("Scan PICC to see UID, SAK, type, and data blocks..."));
	while (1) {
		// Prepare key - all keys are set to FFFFFFFFFFFFh at chip delivery from the factory.
		MFRC522::MIFARE_Key key;
		for (byte i = 0; i < 6; i++)
			key.keyByte[i] = 0xFF;

		// Look for new cards
//		if (!mfrc522.PICC_IsNewCardPresent()) {
//			return;
//		}

		// Select one of the cards
		if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial()) {

			Serial.print(F("Card UID:"));    //Dump UID
			for (byte i = 0; i < mfrc522.uid.size; i++) {
				Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
				Serial.print(mfrc522.uid.uidByte[i], HEX);
			}
			Serial.print(F(" PICC type: "));   // Dump PICC type
			MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
			Serial.println(mfrc522.PICC_GetTypeName(piccType));

			byte buffer[34];
			byte block;
			MFRC522::StatusCode status;
			byte len;

			Serial.setTimeout(20000L); // wait until 20 seconds for input from serial
			// Ask personal data: Family name
			Serial.println(F("Type Family name, ending with #"));
			len = Serial.readBytesUntil('#', (char *) buffer, 30); // read family name from serial
			for (byte i = len; i < 30; i++)
				buffer[i] = ' ';     // pad with spaces

			block = 1;
			//Serial.println(F("Authenticating using key A..."));
			status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A,
					block, &key, &(mfrc522.uid));
			if (status != MFRC522::STATUS_OK) {
				Serial.print(F("PCD_Authenticate() failed: "));
				Serial.println(mfrc522.GetStatusCodeName(status));
				return;
			} else
				Serial.println(F("PCD_Authenticate() success: "));

			// Write block
			status = mfrc522.MIFARE_Write(block, buffer, 16);
			if (status != MFRC522::STATUS_OK) {
				Serial.print(F("MIFARE_Write() failed: "));
				Serial.println(mfrc522.GetStatusCodeName(status));
				return;
			} else
				Serial.println(F("MIFARE_Write() success: "));

			block = 2;
			//Serial.println(F("Authenticating using key A..."));
			status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A,
					block, &key, &(mfrc522.uid));
			if (status != MFRC522::STATUS_OK) {
				Serial.print(F("PCD_Authenticate() failed: "));
				Serial.println(mfrc522.GetStatusCodeName(status));
				return;
			}

			// Write block
			status = mfrc522.MIFARE_Write(block, &buffer[16], 16);
			if (status != MFRC522::STATUS_OK) {
				Serial.print(F("MIFARE_Write() failed: "));
				Serial.println(mfrc522.GetStatusCodeName(status));
				return;
			} else
				Serial.println(F("MIFARE_Write() success: "));

			// Ask personal data: First name
			Serial.println(F("Type First name, ending with #"));
			len = Serial.readBytesUntil('#', (char *) buffer, 20); // read first name from serial
			for (byte i = len; i < 20; i++)
				buffer[i] = ' ';     // pad with spaces

			block = 4;
			//Serial.println(F("Authenticating using key A..."));
			status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A,
					block, &key, &(mfrc522.uid));
			if (status != MFRC522::STATUS_OK) {
				Serial.print(F("PCD_Authenticate() failed: "));
				Serial.println(mfrc522.GetStatusCodeName(status));
				return;
			}

			// Write block
			status = mfrc522.MIFARE_Write(block, buffer, 16);
			if (status != MFRC522::STATUS_OK) {
				Serial.print(F("MIFARE_Write() failed: "));
				Serial.println(mfrc522.GetStatusCodeName(status));
				return;
			} else
				Serial.println(F("MIFARE_Write() success: "));

			block = 5;
			//Serial.println(F("Authenticating using key A..."));
			status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A,
					block, &key, &(mfrc522.uid));
			if (status != MFRC522::STATUS_OK) {
				Serial.print(F("PCD_Authenticate() failed: "));
				Serial.println(mfrc522.GetStatusCodeName(status));
				return;
			}

			// Write block
			status = mfrc522.MIFARE_Write(block, &buffer[16], 16);
			if (status != MFRC522::STATUS_OK) {
				Serial.print(F("MIFARE_Write() failed: "));
				Serial.println(mfrc522.GetStatusCodeName(status));
				return;
			} else
				Serial.println(F("MIFARE_Write() success: "));

			Serial.println(" ");
			mfrc522.PICC_HaltA(); // Halt PICC
			mfrc522.PCD_StopCrypto1();  // Stop encryption on PCD
		}
	}
}

void read_infor(void) {

	Serial.begin(115200);
	while (!Serial);

// Initialize serial communications with the PC
	SPI.begin(18, 19, 23, 2);                                                // Init SPI bus
	mfrc522.PCD_Init();                                     // Init MFRC522 card
	mfrc522.PCD_DumpVersionToSerial();
	Serial.println(F("Read personal data on a MIFARE PICC:"));
	MFRC522::MIFARE_Key key;
	for (byte i = 0; i < 6; i++)
		key.keyByte[i] = 0xFF;

	//some variables we need
	byte block;
	byte len;
	MFRC522::StatusCode status;

	while (1) {

		// Prepare key - all keys are set to FFFFFFFFFFFFh at chip delivery from the factory.

		//-----------------------------------------

		// Select one of the cards
		if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial()) {


	//	Serial.println(F("**Card Detected:**"));

		//-------------------------------------------
	//	mfrc522.PICC_DumpDetailsToSerial(&(mfrc522.uid)); //dump some details about the card

		//mfrc522.PICC_DumpToSerial(&(mfrc522.uid));      //uncomment this to see all blocks in hex

		//-------------------------------------------

		Serial.print(F("Name: "));

		byte buffer1[18];

		block = 4;
		len = 18;

		//------------------------------------------- GET FIRST NAME
		status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 4,
				&key, &(mfrc522.uid)); //line 834 of MFRC522.cpp file
		if (status != MFRC522::STATUS_OK) {
			Serial.print(F("Authentication failed: "));
			Serial.println(mfrc522.GetStatusCodeName(status));
			return;
		}

		status = mfrc522.MIFARE_Read(block, buffer1, &len);
		if (status != MFRC522::STATUS_OK) {
			Serial.print(F("Reading failed: "));
			Serial.println(mfrc522.GetStatusCodeName(status));
			return;
		}

		//PRINT FIRST NAME
		for (uint8_t i = 0; i < 16; i++) {
			if (buffer1[i] != 32) {
				Serial.write(buffer1[i]);
			}
		}
		Serial.print(" ");

		//---------------------------------------- GET LAST NAME

		byte buffer2[18];
		block = 1;

		status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 1,
				&key, &(mfrc522.uid)); //line 834
		if (status != MFRC522::STATUS_OK) {
			Serial.print(F("Authentication failed: "));
			Serial.println(mfrc522.GetStatusCodeName(status));
			return;
		}

		status = mfrc522.MIFARE_Read(block, buffer2, &len);
		if (status != MFRC522::STATUS_OK) {
			Serial.print(F("Reading failed: "));
			Serial.println(mfrc522.GetStatusCodeName(status));
			return;
		}

		//PRINT LAST NAME
		for (uint8_t i = 0; i < 16; i++) {
			Serial.write(buffer2[i]);
		}
		Serial.println(F("\n"));
		//----------------------------------------

	//	Serial.println(F("\n**End Reading**\n"));

		delay(1000); //change value if you want to read cards faster

		mfrc522.PICC_HaltA();
		mfrc522.PCD_StopCrypto1();
	 }
	}
}

