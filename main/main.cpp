#include "freertos/FreeRTOS.h"
#include "sdkconfig.h"
#include "freertos/task.h"
#include <esp_log.h>
#include <string>

#include "GBBLightRGB.h"
#include "GBBodyWheels.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "GBBBuzzer.h"

extern "C" {
void app_main(void);
}

extern void task_initI2C(void*);
extern void task_display(void*);

extern void testBLE(void);
extern void test_audio(void *param);
extern void test_gpio(void);
void test_gpio_2(void);
extern void tao_ngat(void *param);
extern void test_interrupt(void *param);

extern void RFID_duminfor(void);
extern void read_UID(void);
extern void write_infor(void);
extern void read_infor(void) ;

extern void test_motor(void);
extern void init_audio(void);
void config_gpio_motor(void);
extern void   soundMidi(void);
extern void test_audido(void *param);

extern GBBodyWheels Gobot;


static void Gobot_Playing(void *param){
	for(;;){

		Gobot.isRuning();
	}
}







void app_main(void) {

	esp_err_t err = nvs_flash_init();
	if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
		// OTA app partition table has a smaller NVS partition size than the non-OTA
		// partition table. This size mismatch may cause NVS initialization to fail.
		// If this happens, we erase NVS partition and initialize NVS again.
		ESP_ERROR_CHECK(nvs_flash_erase());
		err = nvs_flash_init();
	}
	ESP_ERROR_CHECK(err);


//	xTaskCreate(tao_ngat, "mpu_task", 2048, NULL, 5, NULL);
//
//test_audio(NULL);
//	init_audio();
	// RFID_duminfor();
//	write_infor();
//	read_infor();
	test_audio(NULL);

//	config_gpio_motor();
//	RobotLED.runRGB(ADVERIESING,1);

//	RobotBuzzer.play_file__array(start,12);
//	vtaskdelay(200)


//	RobotBuzzer.play_file__array(bipbip,sizeof(bipbip)/4);


//	testBLE();
//	xTaskCreate(test_led, "mpu_task", 2048, NULL, 5, NULL);
 // test_motor();
	while (1)
		;

}



void config_gpio_motor(void){

	gpio_config_t gpioConfig;

	gpioConfig.intr_type = GPIO_INTR_DISABLE;
	gpioConfig.mode = GPIO_MODE_OUTPUT;
	//backup
//	gpioConfig.pin_bit_mask = GPIO_SEL_21 | GPIO_SEL_14 | GPIO_SEL_12
//			| GPIO_SEL_19 | GPIO_SEL_32;
//
	// new version
	gpioConfig.pin_bit_mask = GPIO_SEL_17 | GPIO_SEL_22 | GPIO_SEL_5
				| GPIO_SEL_21 | GPIO_SEL_12;

	gpio_config(&gpioConfig);
}
